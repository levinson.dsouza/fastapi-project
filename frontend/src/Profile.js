import React from "react";
import {
  AppBar,
  Toolbar,
  Button,
  Box,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TextField,
} from "@mui/material";
import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom"
import { fetchToken } from "./auth/Auth"

function Profile() {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});
  const userId = fetchToken("user")

  //edit this for fetching one user
  const fetchUsers = async () => {
    const response = await axios.get(`http://192.168.13.35:8000/${userId}`);
    const fetchedUsers = []
    fetchedUsers.push(response.data)
    return setUsers(fetchedUsers);
  };

  useEffect(() => fetchUsers(), [])

  const fetchUser = async (id) => {
    const response = await axios.get(`http://192.168.13.35:8000/${id}`, {});
    return setUser(response.data);
  };

  const createOrEditUser = async () => {
    if (user.id) {
      await axios.put(`http://192.168.13.35:8000/${user.id}`, user);
    } else {
      await axios.post(`http://192.168.13.35:8000/`, user);
    }
    await fetchUsers();
    setUser({ id: 0, name: "", email: "", password: "", address: "" });
  };

  const deleteUser = async (id) => {
    await axios.delete(`http://192.168.13.35:8000/${id}`);
    navigate("/signup")
    localStorage.removeItem("jwt");
    localStorage.removeItem("user");
    // await fetchUsers();
  };

  const signOut = () => {
    localStorage.removeItem("jwt");
    localStorage.removeItem("user");
    navigate("/")
  }

  return (
    <div>
      <Button
        onClick={() => signOut()}
        variant="contained"
        color="primary"
      >
        Logout
      </Button>
      <AppBar position="static">
        <Toolbar>
          <Button color="inherit">users</Button>
        </Toolbar>
      </AppBar>
      <Box m={10}>
        <TableContainer>
          <TextField value={user.id} type="hidden" />
          <Table aria-label="simple table">
            <TableBody>
              <TableRow>
                <TableCell>
                  <TextField
                    value={user.name}
                    onChange={(e) => setUser({ ...user, name: e.target.value })}
                    id="standard-basic"
                    label="Name"
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    value={user.email}
                    onChange={(e) => setUser({ ...user, email: e.target.value })}
                    id="standard-basic"
                    label="Email"
                  />
                </TableCell>
                {/* <TableCell>
                  <TextField
                    value={user.password}
                    onChange={(e) => setUser({ ...user, password: e.target.value })}
                    id="standard-basic"
                    label="Password"
                  />
                </TableCell> */}
                <TableCell>
                  <TextField
                    value={user.address}
                    onChange={(e) => setUser({ ...user, address: e.target.value })}
                    id="standard-basic"
                    label="Address"
                  />
                </TableCell>
                <TableCell>
                  <Button
                    onClick={() => createOrEditUser()}
                    variant="contained"
                    color="primary"
                  >
                    Submit
                  </Button>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Email</TableCell>
                {/* <TableCell>Password</TableCell> */}
                <TableCell>Address</TableCell>
                <TableCell>Edit</TableCell>
                <TableCell>Delete</TableCell>
              </TableRow>
              {users.map((row) => (
                <TableRow key={row.id}>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.email}</TableCell>
                  {/* <TableCell>{row.password}</TableCell> */}
                  <TableCell>{row.address}</TableCell>
                  <TableCell>
                    <Button
                      onClick={() => fetchUser(row.id)}
                      variant="contained"
                      color="primary"
                    >
                      Edit
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Button
                      onClick={() => deleteUser(row.id)}
                      variant="contained"
                      color="secondary"
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </div>
  );
}

export default Profile;
