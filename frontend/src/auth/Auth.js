import { useLocation,Navigate } from "react-router-dom"
import React from 'react';

export const setToken = (name, token)=>{
    localStorage.setItem(name, token)
}

export const fetchToken = (token)=>{
    return localStorage.getItem(token)
}

export function RequireToken({children}){

    let auth = fetchToken("jwt")
    let location = useLocation()

    if(!auth){

        return <Navigate to='/' state ={{from : location}}/>;
    }

    return children;
}