import React from 'react';
import { Routes, Route } from "react-router-dom";
import Login from "./Login";
import Profile from "./Profile";
import SignUp from "./SignUp";
import { RequireToken } from "./auth/Auth";

function App(){
    return  <div className ="App">
    <Routes>
      <Route path="/" element = {<Login/>}/>
      <Route path="/signup" element = {<SignUp/>}/>
      <Route 
        path="/profile" 
        element = {
            <RequireToken>
                <Profile/>   
            </RequireToken>
        } />

    </Routes>
    </div>
}

export default App;