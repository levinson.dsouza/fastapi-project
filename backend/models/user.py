from sqlalchemy import Table, Column
from sqlalchemy.sql.expression import true
from sqlalchemy.sql.sqltypes import Integer, String
from config.db import meta, engine

users = Table('users', meta,
    Column('id', Integer, primary_key=true),
    Column('name', String(20)),
    Column('email', String(20)),
    Column('password', String(20)), 
    Column('address', String(40)) 
)
meta.create_all(engine)