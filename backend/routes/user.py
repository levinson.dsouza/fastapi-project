from distutils.log import error
from sqlalchemy.sql.elements import Null
from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
import jwt
import bcrypt
from fastapi.encoders import jsonable_encoder


from models.user import users
from config.db import conn
from schemas.user import User

user = APIRouter()

## JWT Setup
SECERT_KEY = "YOUR_FAST_API_SECRET_KEY"
ALGORITHM ="HS256"
ACCESS_TOKEN_EXPIRES_MINUTES = 800

class LoginItem(BaseModel):
    email: str
    password: str

@user.get('/')
async def fetch_users():
    # conn.execute(users.insert().values(name="rachit", email="rachit@va", address="mumbai"))
    return conn.execute(users.select()).fetchall()


@user.get('/{id}')
async def fetch_user(id: int):
    return conn.execute(users.select().where(users.c.id == id)).first()


@user.post('/')
async def add_user(user: User):
    conn.execute(users.insert().values(
        name=user.name,
        email=user.email,
        password=bcrypt.hashpw(user.password, bcrypt.gensalt(10)),
        address=user.address
    ))
    return conn.execute(users.select()).fetchall()


@user.put('/{id}')
async def update_user(id: int, user: User):
    conn.execute(users.update().values(
        name=user.name,
        email=user.email,
        password=user.password,
        address=user.address
    ).where(users.c.id == id))
    return conn.execute(users.select()).fetchall()


@user.delete('/{id}')
async def delete_user(id: int):
    conn.execute(users.delete().where(users.c.id == id))
    return conn.execute(users.select()).fetchall()


@user.post('/login')
async def user_login(loginitem:LoginItem):
    data = jsonable_encoder(loginitem)
    user = conn.execute(users.select().where(users.c.email == data['email'])).first()
    try:
        if bcrypt.checkpw(data['password'], user['password']):
            encoded_jwt = jwt.encode(data, SECERT_KEY, algorithm=ALGORITHM)
            return {"token": encoded_jwt, "user": user["id"]}
        else:
            raise HTTPException(status_code=404, detail="Password or Email do not match")
    except:
        raise HTTPException(status_code=404, detail="Some error occured")
    # user = meta.Session.query(User).filter(User.email.like(data['email']), User.password.)
    # if data['username']== test_user['username'] and data['password']== test_user['password']:


    # else:
    return {"message":"login failed"}