from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routes.user import user

app = FastAPI()

origins = [
    "http://127.0.0.1:8000",
    "http://192.168.13.22:80",
    "http://192.168.13.22:8000",
    "http://192.168.13.22",
    "http://192.168.13.25:8000",
    "http://192.168.13.25",
    "http://192.168.13.35:80",
    "http://192.168.13.35:8000",
    "http://192.168.13.35",
    "http://localhost:3000",
    "http://localhost:3001",
    "http://localhost:8000",
    "http://localhost:8080",
    "http://localhost",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# app.add_middleware(
#     CORSMiddleware,
#     allow_origins="http://127.0.0.1:8000",
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )

app.include_router(user)
