from sqlalchemy import create_engine, MetaData

# engine = create_engine("mysql+pymysql://root:pass@localhost:3306/fullstackApp")
# engine = create_engine("mysql+pymysql://root:pass@192.168.13.25:33060/fullstackApp")


# Docker container address
# engine = create_engine("mysql+pymysql://root:pass@172.17.0.2:3306/fullstackApp")


engine = create_engine("mysql+pymysql://root:pass@192.168.13.26:3306/fullstackApp")

meta = MetaData()
conn = engine.connect()