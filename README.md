# User Database project V1.01 (FastAPI + React.js)

### This was a project undertaken by Rachit Bubna and Levinson D'souza as a part of their internship with Visible Alpha. This is a project which is built keeping Devops practices as priority and hence this project is being deployed and monitored within containers hosted using nginx reverse proxy within VM. 

### Steps to Run this project on your local machine

#### Note: Docker and Docker-Compose are required to run this project, please make sure to have both up and running before moving forward. use docker -v and docker-compose -v to confirm.

### 1. Clone the git repository to download the source code using
```
$ git clone https://gitlab.com/levinson.dsouza/fastapi-project.git
```

### 2. CD into the created directory  
```
$ cd fastapi-project/
```

### 3. Run the following command to build the project, it will download the required dependencies
```
$ docker-compose build
```

### 4. Run the following command to run the project
```
$ docker-compose up
```
